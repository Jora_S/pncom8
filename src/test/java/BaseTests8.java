import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: georgiy
 * Date: 24.04.18
 * Time: 11:22
 * To change this template use File | Settings | File Templates.
 */
public class BaseTests8 {



    WebDriver driver = initChromedriver();
    @Test
    public void Test1() {
        driver.get("https://pn.com.ua/");
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement poisk = driver.findElement(By.xpath("//*[@id=\"search\"]/div[1]/div[1]/input"));
        poisk.sendKeys("iphone");
        WebElement button = driver.findElement(By.cssSelector("#search > div.search-group > div.search-group-submit > input[type=\"submit\"]")) ;
        button.click();
        WebElement spisok = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div/div[2]/div/section/article[2]/ul/li[1]/ul/li[1]/ul/li/a"));
        spisok.click();
        WebElement model = driver.findElement(By.xpath("//*[@id=\"column-center\"]/section/div[3]/article[2]/div[2]/div[1]/a"));
        model.click();
        WebElement prise = driver.findElement(By.xpath("//*[@id=\"average-price\"]/div[1]/span[1]"));
        String priseText = prise.getText();
        assertEquals("16 400 грн", priseText) ;
        WebElement proizvoditel = driver.findElement(By.xpath("//*[@id=\"main-block\"]/div[1]/div[2]/div[3]/table/tbody/tr[22]/td/a"));
        proizvoditel.click();
        WebElement sravnit = driver.findElement(By.className("add-to-compare-link"));
        sravnit.click();
        WebElement button1 = driver.findElement(By.xpath("//*[@id=\"main-block\"]/div[1]/div[2]/div[2]/div[2]/div/span[1]/a[2]"));
        button1.click();
        WebElement iphone = driver.findElement(By.xpath("//*[@id=\"mCSB_1_container\"]/div[1]/div/div/div[2]/div[1]/a"));
        String iphoneText = iphone.getText();
        assertEquals("Apple iPhone 7 32Gb", iphoneText);
        WebElement korzina = driver.findElement(By.xpath("/html/body/div[1]/header/div/div[2]/div[2]/nav/ul/li[2]/span/a"));
        korzina.click();
        WebElement tovari = driver.findElement(By.xpath("/html/body/div[1]/header/div/div[2]/div[1]/nav/ul/li[1]/a/span"));
        tovari.click();
        WebElement skovorodi = driver.findElement(By.cssSelector("#column-center > section > div:nth-child(4) > ul > li:nth-child(1) > a"));
        skovorodi.click();
        WebElement skovorodka = driver.findElement(By.xpath("//*[@id=\"column-center\"]/section/div[3]/ul/li[1]/article/div[2]/div[1]/a"));
        skovorodka.click();
        WebElement prise2 = driver.findElement(By.xpath("//*[@id=\"average-price\"]/div[1]/span[1]"));
        String prise2Text = prise2.getText();
        assertEquals("431 грн", prise2Text);
        WebElement color = driver.findElement(By.xpath("//*[@id=\"main-block\"]/div[1]/div[2]/div[3]/table/tbody/tr[8]/td[2]"));
        String colorText = color.getText();
        assertEquals("черный", colorText);
        driver.quit();

    }


    public static WebDriver initChromedriver(){
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver.exe");
        return new ChromeDriver() ;
    }
}
